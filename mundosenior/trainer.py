import lda
import json

from flask_restful import Resource, reqparse
from flask import request

from json_tricks import dumps

import numpy as np


class Train(Resource):

	def __init__(self):
		post_parser = reqparse.RequestParser()
		post_parser.add_argument('n_topics', dest = 'n_topics',
			type = int, location = 'args',
			default = 5, help = 'Number of topics to LDA algorithm',)
		post_parser.add_argument('alpha', dest = 'alpha',
			type = float, location = 'args',
			default = 0.1, help = 'Alpha parameter for the LDA algorithm',)
		post_parser.add_argument('betha', dest = 'betha',
			type = float, location = 'args',
			default = 0.1, help = 'Betha parameter for the LDA algorithm',)
		args = post_parser.parse_args()

		self.dict_categories = {}
		self.n_topics = args.n_topics
		self.alpha = args.alpha
		self.betha = args.betha

		print("topics",args.n_topics)
		print("alpha",args.alpha)
		print("betha",args.betha)

	def train_lda(self, X_data, features_names):
		"""
		This function train a LDA model.

		Parameters:
		X_data -- numpy matrix
		features_names -- list of strings

		Return:
		Dictionary with the topics and top 10 words in each topic.

		"""

		print("Training lda model")

		assert X_data.dtype == "int64" or X_data.dtype == "int32" or X_data.dtype == "int16", "The matrix must to be of integers to train LDA model. Try to set 'idf' param of vectorization to false."

		#define and train lda model to X_data given by argument
		lda_model = lda.LDA(n_topics = self.n_topics, alpha = self.alpha, eta = self.betha, n_iter = 500)
		
		X_topics = lda_model.fit_transform(X_data)

		#get the topic of every document
		_lda_keys = []
		for i in range(X_topics.shape[0]):
			_lda_keys +=  X_topics[i].argmax(),

		topic_word = lda_model.components_ 

		topic_summaries = []
		topics = []

		for i, topic_dist in enumerate(topic_word):
			dict_data = {}

			#dado que los datos estan ordenados de menor a mayor se escogen los ultimos (mayor probabilidad)
			topic_words = np.array(features_names)[np.argsort(topic_dist)][:-(10 + 1):-1] # get!
			words_prob = topic_dist[np.argsort(topic_dist)][:-(10 + 1):-1]
			#topic_summaries.append(' '.join(topic_words)) # append!

			dict_data["id"] = i
			dict_data["nombre"] = "Topic " + str(i)
			dict_data["empleos"] = list(topic_words)
			dict_data["probabilities"] = list(words_prob)

			topics.append(dict_data)

		print("topics",topics)
		print("_lda_keys",(_lda_keys))

		dict_categories = dict({"categories" : topics, "cluster_ref": dumps(_lda_keys)})

		return dict_categories

	def post(self):
		print("Not implemented")
		pass

	def put(self):
		print("PUT trainer method")
		json_data = request.get_json(force=True)

		data = np.array(json.loads(json_data["vectorized_data"]))
		self.dict_categories = self.train_lda(data, json_data["features_names"])
		
		return self.dict_categories

	def get(self):
		print("Not implemented")
		pass

	def delete(self):
		print("Not implemented")
		pass






































































