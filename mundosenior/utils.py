import json

class Utils():
	
	@staticmethod
	def parse_json_classifieds(file_json):
		"""
		This functions is to parse a json to
		"""

		titles = []
		description = []
		category = []
		profession = []
		salary = []
		with open(file_json) as jsonfile:
			data = json.load(jsonfile)
			for row in data:
				titles.append(row["titulo"])
				description.append(row["descripcion"])
				category.append(row["categoria"])
				profession.append(row["keywordsProfesiones"])
				salary.append(row["sueldo"])

		return titles, description, category, profession, salary

	@staticmethod
	def parse_json_classifieds_computrabajo(file_json):
		"""
		This functions is to parse a json to
		"""

		titles = []
		descriptions = []
		org_names = []
		org_regions = []
		org_citys = []
		valorations = []
		with open(file_json) as jsonfile:
			data = json.load(jsonfile)
			for row in data:
				titles.append(row["title"])
				descriptions.append(row["description"])
				org_names.append(row["organization_name"])
				org_regions.append(row["organization_region"])
				org_citys.append(row["organization_city"])
				valorations.append(row["valoration"])

		return titles, descriptions, org_names, org_regions, org_citys, valorations

	@staticmethod
	def parse_json_offers(file_json):
		names = []
		descriptions = []
		profiles = []
		localities = []
		professions = []
		salaries = []
		with open(file_json) as jsonfile:
			data = json.load(jsonfile)
			for row in data:
				names.append(row["nombre"])
				descriptions.append(row["descripcion"])
				profiles.append(row["perfilPostulante"])
				localities.append(row["localidad"])
				professions.append(row["keywordsProfesiones"])
				salaries.append(row["sueldo"])
				

		return names, descriptions, profiles, localities, professions, salaries