from flask import Flask
from flask_restful import Api

app = Flask(__name__)
api = Api(app)

from mundosenior.data_process import Data
from mundosenior.data_process import Vectorizer
from mundosenior.trainer import Train
from mundosenior.utils import Utils

api.add_resource(Data, '/data/')
api.add_resource(Vectorizer, '/vect/', endpoint = 'vect')
api.add_resource(Train, '/train/')