from mundosenior import app
from mundosenior import api
from mundosenior.utils import Utils

import numpy as np
import json
import time
import nltk
import string
import sklearn

from json_tricks import dump, dumps, load, loads, strip_comments

from flask import jsonify, request
from flask_restful import Resource, reqparse, inputs

class Data(Resource):

	def __init__(self):
		post_parser = reqparse.RequestParser()
		post_parser.add_argument('filename', dest = 'filename',
			type = str, location = 'args',
			default = "/Users/cristobal/Documents/MundoSeniorAPI/Data/offers.json", help = 'Filename in json format.',)
		args = post_parser.parse_args()
		
		self.filename = args.filename
		print("Filename: ", self.filename)
		try:
			self.data = self.read_data(self.filename)
		except:
			print("Error in filename lecture")

	def read_data(self, filename):
		print("entre e read_data")
		if "offers" in filename:
			names, descriptions, profiles, localities, professions, salaries = Utils.parse_json_offers(filename)
			name_field_dict = {"data" : names}

		if "classifieds" in filename:
			titles, descriptions, categorys, professions, salarys = Utils.parse_json_classifieds(filename)
			name_field_dict = {"data" : titles}

		if "compu" in filename:
			print("ENTRE A COMPU")
			titles, descriptions, org_names, org_regions, org_citys, valorations = Utils.parse_json_classifieds_computrabajo(filename)
			print(titles)
			name_field_dict = {"data" : titles}
	
		return name_field_dict

	def put(self, filename):
		self.filename = filename
		return self.filename

	def post(self):
		print("Not implemented")
		pass

	def delete(self):
		print("Not implemented")
		pass

	def get(self):
		print("GET data")
		return self.data

class Vectorizer(Resource):

	def __init__(self):
		#this inicialization of the Data is a patch
		print("Vectorizer constructor")

		post_parser = reqparse.RequestParser()
		post_parser.add_argument('binar', dest = 'binar',
			type = inputs.boolean, location = 'args',
			default = True, help = 'Make binar vectorization',)
		post_parser.add_argument('idf', dest = 'idf',
			type = inputs.boolean, location = 'args',
			default = False, help = 'Weighted vectorization with idf',)
		
		args = post_parser.parse_args()
		print("args binar: ", args.binar)
		print("args idf: ", args.idf)

		self.binar = args.binar
		self.idf = args.idf

		self.vectorized_data = []
		self.features_names = []

	def process_text(self, text):
		"""
		This function pre processes a text statement.
		The elimination of stopwords is not considered, because it is carried out to the feature extraction stage.

		The pre-processing considers:
		- Tokenize
		- Lower case
		- Remove punctuation
		- Remove alpha numeric characters.

		Parameters:
		text -- string type text to be pre-processed 

		Return:
		List with pre-processed words in text

		"""

		#tokenize text
		tokens = nltk.word_tokenize(text)    

		#every token is transform to lower case
		lower_tokens = [w.lower() for w in tokens]

		#removing punctuation
		table = str.maketrans('', '', string.punctuation)
		stripped = [w.translate(table) for w in lower_tokens]

		#removing stopwords.
		for token in stripped:
			if token in nltk.corpus.stopwords.words('spanish'):
				stripped.remove(token)

		#removing alpha numeric 
		return [word for word in stripped if word.isalpha()]

	def vectorization(self, text_data, binar, idf):
		"""
		This function vectorize a list of documents.
		This function call process_text function to process the text.

		Parameters:
		text_data -- Pandas dataframe series with the data to vectorize.
		args -- arguments given by command line.

		Return:
		X_data -- matrix with vectorized data. Shape [samples, features]
		Features_names -- names of the features in the matrix.

		"""

		bow_transformer = sklearn.feature_extraction.text.CountVectorizer(analyzer = self.process_text,
									stop_words = nltk.corpus.stopwords.words('spanish'),
									min_df = round(len(text_data)*0.0025),
									ngram_range = (1,2),
									binary = binar)

		#calculate time of vectorization
		start_time = time.time()
		X_data = bow_transformer.fit_transform(text_data)
		#X_data = bow_transformer.transform(text_data)
		print("Time of vectorization is %f seconds" %(time.time() - start_time))

		#print(X_data)
		features_names = bow_transformer.get_feature_names()
		#print(features_names)

		if idf: #True args.idf
			print("Doing IDF matrix")
			#Here the documents are weighted
			tfidf_transformer = sklearn.feature_extraction.text.TfidfTransformer()
			X_data = tfidf_transformer.fit_transform(X_data)
			print("number of features is %i seconds" %X_data.shape[1])
		
		#X_data must to be serialized to json
		return json.dumps(X_data.toarray().tolist()), features_names

	def put(self):
		print("PUT method vect")
		#get json data with request
		json_data = request.get_json(force=True)

		#using vectorization method
		self.vectorized_data, self.features_names = self.vectorization(json_data["data"], self.binar, self.idf)
		return {"vectorized_data" : self.vectorized_data, "features_names" : self.features_names}

	def post(self):
		print("Not implemented")
		pass

	def delete(self):
		print("Not implemented")
		pass

	def get(self):
		print("Not implemented")
		pass























