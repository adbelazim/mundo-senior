from requests import get, put
import json


	

def read_params(filename):
	with open(filename) as f:
		data = json.load(f)
	
	return data

params = read_params("params.json")
print(params["Data"]["filename"])
print(bool(params["Vectorizer"]["binar"]))
#print(bool(params["Vectorizer"]["idf"]))
print(type(params["Trainer"]["n_topics"]))
print(type(params["Trainer"]["alpha"]))
print(type(params["Trainer"]["betha"]))
#lectura de datos
print("lectura de datos")
data_lect = get('http://127.0.0.1:5000/data/', params = {'filename' : params["Data"]["filename"]}).json()

#post vectorizer
print("vectorizer")
vect = put('http://127.0.0.1:5000/vect/', params = {'binar': bool(params["Vectorizer"]["binar"]), 'idf': False}, json = data_lect).json()

print("trainer")
model = put('http://127.0.0.1:5000/train/', params = {'n_topics': params["Trainer"]["n_topics"], 'alpha': params["Trainer"]["alpha"], 'betha': params["Trainer"]["betha"]}, json = vect).json()

