### Aplicación Mundo Senior ###

Esta aplicación es la implementación de un API rest para vectorizar y entrenar un modelo LDA para una lista de documentos.
Los documentos en esta aplicación se refieren a títulos de clasificados y ofertas.

El back-end está desarrollado con Flask, tanto la vectorización como entrenamiento con las librerias numpy, scipy, sklearn y lda.

### Instalación de dependencias.

Para instalar las dependencias de las librerias utilizadas en el desarrollo del API se debe utilizar el siguiente comando:
	pip3 install -r requirements.txt

El archivo requirements.txt contiene todas las dependencias. Se recomienda utilizar virtualenv con python3 para instalar las dependencias.

El despliegue de la aplicación se realiza con el siguiente comando:
	FLASK_APP=msenior.py flask run

## Contacto

cristobal.vasquez@usach.cl